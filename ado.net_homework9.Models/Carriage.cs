﻿using ado.net_homework9.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ado.net_homework9.Models
{
    public class Carriage:Entity
    {
      string Type { get; set; }
      string Number { get; set; }
      int FreeSeats { get; set; }
    }
}
